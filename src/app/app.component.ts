import { Component } from '@angular/core';
import { Productos } from './models/productos.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 arrComida:Productos[];
 arrBebida: Productos[];
 arrProductSeleccionado:Productos[];

 constructor(){

  this.arrProductSeleccionado= [];
   this.arrComida= [
     new Productos('Carne en salsa','https://fitenium.com/wp-content/uploads/2020/10/2560_3000-256x256.jpg',23900),
     new Productos('Hamburguesa','https://images.rappi.com/new_store_type/restaurant-1601669046.png?d=200x200&e=webp',15000),
     new Productos('Empanadas','http://www.corporacionvital.com/corporacionvital/dico/fotos/69_2.jpeg',2000),
     new Productos('Pollo Asado','https://ipcdn.freshop.com/resize?url=https://images.freshop.com/1564405684704628848/34a4388a8396c5f50760b63bb5ab886a_large.png&width=256&type=webp&quality=40',26500),
     new Productos('Combo papas + hamburguesa','https://i0.wp.com/goula.lat/wp-content/uploads/2019/12/hamburguesa-beyond-meat-scaled-e1577396155298.jpg?fit=1600%2C1068&ssl=1',19500),
     new Productos('Pollo dulce','https://images.rappi.com/restaurants_background/home-1597425813299.png?d=128x80',16000)
    ] 

    this.arrBebida=[
      new Productos('Cocala','https://images.rappi.com/products/2091668051-1594685807959.png?d=136x136',2500),
      new Productos('limonada','https://images.rappi.com/products/1e4c823e-231a-4aad-9153-829fb069f5c5-1596824985537.jpeg?d=128x90',3500),
      new Productos('coctel','https://i.pinimg.com/474x/f3/56/37/f356373ac73a3826b0160ced1b1f4524.jpg',7500)

    ]
 }

 onProductoSeleccionado(event){
   //this.arrProductSeleccionado.push($event);
  const productoEncontrado= this.arrProductSeleccionado.find(producto=> producto.nombre===event.nombre);
  if(productoEncontrado){

    productoEncontrado.cantidad++;
  }else{
    event.cantidad=1;
    this.arrProductSeleccionado.push(event);
  }
  

 }
 
}
