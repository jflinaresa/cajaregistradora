import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';import { Productos } from 'src/app/models/productos.model';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class LIstaProductosComponent implements OnInit {

  @Input() titulo:string;
  @Input() productos: Productos[];
  @Output () productoSeleccionado:EventEmitter<Productos>;
  constructor() { 

    this.productoSeleccionado = new EventEmitter();
  }

  ngOnInit(): void {
  }

  onClick(pProducto){
this.productoSeleccionado.emit(pProducto);
  }

}
